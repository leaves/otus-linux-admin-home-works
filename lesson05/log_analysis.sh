#!/bin/bash

# wrong auth, forbidden acces from nginx access.log
#    432 95.163.86.82
#      5 85.202.229.17
#      1 87.229.181.126
grep -E "\s(401|403)\s[0-9]+\s" /var/log/nginx/access.log | cut -d" " -f 1 | sort | uniq -c | sort -nr


# failed auth from secure log
#     18 0101	 5.188.10.180
#     14 admin	 5.188.10.176
#      6 root	 211.210.104.242
#      5 user	 5.188.10.176
awk '/Failed password for/ {print $(NF-5)"\t",$(NF-3)}' /var/log/secure | sort | uniq -c | sort -n -k 1 -r | head