#!/bin/bash
check_period=30

lock_file='./lock'

# check reuired argument
if [[ -z $1 ]]; then
	echo "pid file required"
	exit 1
else
	if [ ! -f $1 ]; then
		echo "pid file not exist"
		exit 1
	fi
fi

# set check period
if [[ -n $2 ]]; then check_period=$2; fi


# check lock file
if [ -f $lock_file ]; then echo "monitoring is already running"; exit 0; fi

touch $lock_file
trap 'rm -f $lock_file; exit $?' EXIT TERM KILL INT

echo "monitor pid file $1 with check period $check_period"

# check nginx process
while [[ 1 ]]; do
	if [[ -z "$(ps -p `cat $1` -o comm=)" ]]; then
		systemctl restart nginx
		
		message="`date` nginx is restarted"
		echo "$message"

		if [[ -n $3 ]]; then echo -e "$message" | sendmail -f "nginxserver@exmaple.com" -t "$3"; fi
	fi
	sleep $check_period
done
