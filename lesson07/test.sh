#!/bin/bash
rm -f raw_*
if [ ! -f ./raw01 ]; then
	dd if=/dev/urandom of=./raw01 count=256 bs=4M > /dev/null 2>&1 &
fi
if [ ! -f ./raw01 ]; then
	dd if=/dev/urandom of=./raw02 count=256 bs=4M > /dev/null 2>&1 &
fi
wait

ionice -c3 dd if=./raw01 of=./raw_1 &
ionice -c2 -n5 dd if=./raw02 of=./raw_2 &
wait

time nice -n19 gzip raw_1 &
time nice -n5 gzip raw_2 &
wait
