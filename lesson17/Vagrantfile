# -*- mode: ruby -*-
# vim: set ft=ruby :

MACHINES = {
    :inetRouter => {
        :net => [
           {ip: '192.168.255.1', adapter: 2, netmask: "255.255.255.252", virtualbox__intnet: "router-net"},
        ]
    },
# cental
    :centralRouter => {
        :net => [
           {ip: '192.168.255.2', adapter: 2, netmask: "255.255.255.252", virtualbox__intnet: "router-net"},
           {ip: '192.168.0.1', adapter: 3, netmask: "255.255.255.240", virtualbox__intnet: "dir-net"},
           {ip: '192.168.0.33', adapter: 4, netmask: "255.255.255.240", virtualbox__intnet: "hw-net"},
           {ip: '192.168.0.65', adapter: 5, netmask: "255.255.255.192", virtualbox__intnet: "mgt-net"},
           {ip: '192.168.2.2', adapter: 6, netmask: "255.255.255.192", virtualbox__intnet: "dev-net"},
           {ip: '192.168.1.2', adapter: 7, netmask: "255.255.255.192", virtualbox__intnet: "dev-net"}
        ]
    },
    :centralServer => {
        :net => [
           {ip: '192.168.0.2', adapter: 2, netmask: "255.255.255.240", virtualbox__intnet: "dir-net"},
        ]
    },
# office 1
    :office1Router => {
        :net => [
            {ip: '192.168.2.1', adapter: 2, netmask: "255.255.255.192", virtualbox__intnet: "dev-net"},
            {ip: '192.168.2.65', adapter: 3, netmask: "255.255.255.192", virtualbox__intnet: "test-net"},
            {ip: '192.168.2.129', adapter: 4, netmask: "255.255.255.192", virtualbox__intnet: "mng-net"},
            {ip: '192.168.2.193', adapter: 5, netmask: "255.255.255.192", virtualbox__intnet: "hw-net"}
        ]
    },
    :office1Server => {
        :net => [
            {ip: '192.168.2.66', adapter: 2, netmask: "255.255.255.192", virtualbox__intnet: "test-net"},
        ]
    },
# office 2
    :office2Router => {
        :net => [
            {ip: '192.168.1.1', adapter: 2, netmask: "255.255.255.128", virtualbox__intnet: "dev-net"},
            {ip: '192.168.1.129', adapter: 3, netmask: "255.255.255.192", virtualbox__intnet: "test-net"},
            {ip: '192.168.1.193', adapter: 4, netmask: "255.255.255.192", virtualbox__intnet: "hw-net"}
        ]
    },
    :office2Server => {
        :net => [
            {ip: '192.168.1.130', adapter: 2, netmask: "255.255.255.192", virtualbox__intnet: "test-net"},
        ]
    }
}

Vagrant.configure("2") do |config|

  MACHINES.each do |boxname, boxconfig|

    config.vm.define boxname do |box|

        box.vm.box = "centos/7"
        box.vm.host_name = boxname.to_s

        boxconfig[:net].each do |ipconf|
          box.vm.network "private_network", ipconf
        end

        if boxconfig.key?(:public)
          box.vm.network "public_network", boxconfig[:public]
        end

        box.vm.provision "shell", run: "always", inline: <<-SHELL
        yum install traceroute -y
        SHELL

        case boxname.to_s
        when "inetRouter"
          box.vm.provision "shell", run: "always", inline: <<-SHELL
            echo net.ipv4.conf.all.forwarding=1 >> /etc/sysctl.conf
            sysctl -p
            iptables -t nat -A POSTROUTING ! -d 192.168.0.0/16 -o eth0 -j MASQUERADE
            echo '192.168.0.0/16 via 192.168.255.2' >> /etc/sysconfig/network-scripts/route-eth1
            systemctl restart network
            SHELL
        # central
        when "centralRouter"
          box.vm.provision "shell", run: "always", inline: <<-SHELL
            echo net.ipv4.conf.all.forwarding=1 >> /etc/sysctl.conf
            sysctl -p
            echo "DEFROUTE=no" >> /etc/sysconfig/network-scripts/ifcfg-eth0
            echo "GATEWAY=192.168.255.1" >> /etc/sysconfig/network-scripts/ifcfg-eth1
            echo "ADDRESS0=192.168.2.0\nNETMASK0=255.255.255.0\nGATEWAY0=192.168.2.1" >> /etc/sysconfig/network-scripts/route-eth5
            echo "ADDRESS0=192.168.1.0\nNETMASK0=255.255.255.0\nGATEWAY0=192.168.1.1" >> /etc/sysconfig/network-scripts/route-eth6
            systemctl restart network
            SHELL
        when "centralServer"
          box.vm.provision "shell", run: "always", inline: <<-SHELL
            echo "DEFROUTE=no" >> /etc/sysconfig/network-scripts/ifcfg-e
            echo "GATEWAY=192.168.0.1" >> /etc/sysconfig/network-scripts/ifcfg-eth1
            systemctl restart network
            SHELL
        # office 1
        when "office1Router"
          box.vm.provision "shell", run: "always", inline: <<-SHELL
            echo net.ipv4.conf.all.forwarding=1 >> /etc/sysctl.conf
            sysctl -p
            echo "DEFROUTE=no" >> /etc/sysconfig/network-scripts/ifcfg-eth0
            echo 'GATEWAY=192.168.1.2' >> /etc/sysconfig/network-scripts/ifcfg-eth1
            systemctl restart network
            SHELL
        when "office1Server"
          box.vm.provision "shell", run: "always", inline: <<-SHELL
            echo net.ipv4.conf.all.forwarding=1 >> /etc/sysctl.conf
            sysctl -p
            echo "DEFROUTE=no" >> /etc/sysconfig/network-scripts/ifcfg-eth0
            echo 'GATEWAY=192.168.2.65' >> /etc/sysconfig/network-scripts/ifcfg-eth1
            systemctl restart network
            SHELL
        # office 2
        when "office2Router"
          box.vm.provision "shell", run: "always", inline: <<-SHELL
            sysctl net.ipv4.conf.all.forwarding=1
            echo "DEFROUTE=no" >> /etc/sysconfig/network-scripts/ifcfg-eth0
            echo 'GATEWAY=192.168.1.2' >> /etc/sysconfig/network-scripts/ifcfg-eth1
            systemctl restart network
            SHELL
        when "office2Server"
          box.vm.provision "shell", run: "always", inline: <<-SHELL
            echo "DEFROUTE=no" >> /etc/sysconfig/network-scripts/ifcfg-eth0
            echo 'GATEWAY=192.168.1.129' >> /etc/sysconfig/network-scripts/ifcfg-eth1
            systemctl restart network
            SHELL
        end

      end

  end

end
