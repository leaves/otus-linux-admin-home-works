#!/bin/bash

BORG_SERVER="borg@172.16.238.2"
REPO="WWWRepo"
NAME="www-{now:%Y-%m-%d_%H:%M:%S}"

borg create --stats --list $BORG_SERVER:$REPO::$NAME /var/www
