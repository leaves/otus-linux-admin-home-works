#!/bin/bash

config_dir="$HOME/site-storage"

mkdir $config_dir
cd $config_dir

ceph-deploy new ceph-srv01 ceph-srv02 ceph-srv03 ceph-srv04

ceph-deploy install ceph-srv01 &
ceph-deploy install ceph-srv02 &
ceph-deploy install ceph-srv03 &
ceph-deploy install ceph-srv04 &
wait

ceph-deploy mon create-initial
ceph-deploy admin ceph-srv01 ceph-srv02 ceph-srv03 ceph-srv04
ceph-deploy mgr create ceph-srv01 ceph-srv02 ceph-srv03 ceph-srv04

ceph-deploy osd create --data /dev/sdb ceph-srv01
ceph-deploy osd create --data /dev/sdb ceph-srv02
ceph-deploy osd create --data /dev/sdb ceph-srv03
ceph-deploy osd create --data /dev/sdb ceph-srv04

ceph-deploy mds create ceph-srv01 ceph-srv02 ceph-srv03 ceph-srv04
