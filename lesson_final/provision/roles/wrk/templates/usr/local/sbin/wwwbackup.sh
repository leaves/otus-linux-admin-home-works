#!/bin/bash

BORG_SERVER="borg@borg"
REPO="WWWRepo"
NAME="www-{now:%Y-%m-%d_%H:%M:%S}"

borg create --stats --list $BORG_SERVER:$REPO::$NAME /var/www
