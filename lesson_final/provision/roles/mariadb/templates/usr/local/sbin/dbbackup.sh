#!/bin/bash

BORG_SERVER="borg@borg"
REPO="DBRepo"
NAME="db-{now:%Y-%m-%d_%H:%M:%S}"

rm -rf /opt/backup

mariabackup --target-dir /opt/backup --backup
mariabackup --target-dir /opt/backup --prepare

borg create --stats --list $BORG_SERVER:$REPO::$NAME /opt/backup
