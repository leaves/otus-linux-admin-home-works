### requirements
- pyrhon2.7
- virtualenv
- vagrant
- virtualbox

### installation
- pip install -r pip-requirements.cfg
- create hostonly network in virtualbox, 172.16.239.1/24
- add hostonly network name to environment, export HOSTONLY_NETWORK=vboxnet3
- run ```vagrant up``` or ```bash full_up.sh```
- register zabbix agents ```ansible-playbook provision/register_zabbix_agents.yml```

### access
- blog address http://172.16.239.254/
- db host db.site-cluster.internal:3306, db_name: blog, pass: blog, user: blog
- haproxy stats http://172.16.239.254:1936/
- zabbix http://172.16.239.4/zabbix/, login: Admin password: zabbix
- kibana http://172.16.239.4:5601/

tested with Vagrant 2.1.2, VirtualBox 5.2.12