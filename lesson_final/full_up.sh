#!/bin/bash
# vagrant destroy -f

vagrant up --no-provision bind01 &
vagrant up --no-provision bind02 &
vagrant up --no-provision logger &
vagrant up --no-provision lb01 &
vagrant up --no-provision lb02 &
wait
vagrant up --no-provision cache01 &
vagrant up --no-provision cache02 &
vagrant up --no-provision wrk01 &
vagrant up --no-provision wrk02 &
wait
vagrant up --no-provision db01 &
vagrant up --no-provision db02 &
# vagrant up --no-provision db03 &
vagrant up --no-provision maxscale01 &
vagrant up --no-provision maxscale02 &
wait
vagrant up --no-provision ceph-srv01 &
vagrant up --no-provision ceph-srv02 &
vagrant up --no-provision ceph-srv03 &
wait
vagrant up --no-provision ceph-srv04 &
vagrant up --no-provision ceph-admin &
vagrant up --no-provision borg &
wait

vagrant provision bind01 bind02
ansible-playbook provision/main.yml -l '!logger' &
ansible-playbook provision/main.yml -l 'logger' &
wait
