#!/bin/bash

vagrant destroy -f ceph-srv01 &
vagrant destroy -f ceph-srv02 &
vagrant destroy -f ceph-srv03 &
vagrant destroy -f ceph-srv04 &
vagrant destroy -f ceph-admin &
vagrant destroy -f wrk01 &
wait

vagrant up --no-provision ceph-srv01 &
sleep 1
vagrant up --no-provision ceph-srv02 &
sleep 1
vagrant up --no-provision ceph-srv03 &
sleep 1
vagrant up --no-provision ceph-srv04 &
sleep 1
vagrant up --no-provision ceph-admin &
vagrant up wrk01 &
wait

ansible-playbook provision/main.yml -D
