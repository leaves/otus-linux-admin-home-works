%define version 2.16.1
%undefine _disable_source_fetch

Name:           phing
Version:        %{version}
Release:        1%{?dist}
Summary:        PHing Is Not GNU make

License:        GNU LESSER GENERAL PUBLIC LICENSE Version 3
URL:            https://getcomposer.org/
Source0:        https://www.phing.info/get/phing-%{version}.phar

BuildArch:      noarch
Requires:       php

%description
Phing is a PHP project build system or build tool based on ​Apache Ant.

%prep
mv %{_topdir}/SOURCES/phing-%{version}.phar %{_topdir}/SOURCES/phing

%install
mkdir -p $RPM_BUILD_ROOT/usr/local/bin/
cp $RPM_SOURCE_DIR/phing $RPM_BUILD_ROOT/usr/local/bin/phing
exit 0

%make_install
echo make_install
exit 0

%files
%defattr(0755,root,root)

/usr/local/bin/phing
