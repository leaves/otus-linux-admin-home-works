# rpm repo  
vagrant up  
[repo](http://127.0.0.1:8080)

# docker
docker build -t static_site .  
docker run -d -p 8081:80 --name static_site static_site  
[static site](http://127.0.0.1:8081)