%define version 1.7.2
%undefine _disable_source_fetch

Name:           composer
Version:        %{version}
Release:        1%{?dist}
Summary:        Composer helps you declare, manage, and install dependencies of PHP projects.

License:        MIT
URL:            https://getcomposer.org/
Source0:        https://getcomposer.org/download/%{version}/composer.phar

BuildArch:      noarch
Requires:       php

%description
Dependency Manager for PHP

%prep
mv %{_topdir}/SOURCES/composer.phar %{_topdir}/SOURCES/composer

%install
mkdir -p $RPM_BUILD_ROOT/usr/local/bin/
cp $RPM_SOURCE_DIR/composer $RPM_BUILD_ROOT/usr/local/bin/composer
exit 0

%make_install
echo make_install
exit 0

%files
%defattr(0755,root,root)

/usr/local/bin/composer
